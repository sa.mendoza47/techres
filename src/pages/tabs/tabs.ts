import { Component } from '@angular/core';

import { HomePage } from '../1-home/home';
import { InvitacionPage}  from "../2-invitacion/invitacion";
import  {AlertasPage } from "../3-alertas/alertas";
import { NotificacionesPage}  from "../4-notificaciones/notificaciones";
import  {ReservacionPage } from "../5-reservacion/reservacion";


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = InvitacionPage;
  tab3Root = AlertasPage;
  tab4Root = NotificacionesPage;
  tab5Root = ReservacionPage;

  constructor() {

  }
}
