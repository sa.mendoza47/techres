import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/1-home/home';
import { InvitacionPage } from "../pages/2-invitacion/invitacion";
import { AlertasPage } from "../pages/3-alertas/alertas";
import { NotificacionesPage } from "../pages/4-notificaciones/notificaciones";
import { ReservacionPage } from "../pages/5-reservacion/reservacion";
import { TabsPage } from "../pages/tabs/tabs";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { environment } from '../enviroments/enviroments-dev';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    InvitacionPage,
    AlertasPage,
    NotificacionesPage,
    ReservacionPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment),
    AngularFirestoreModule,
    IonicModule.forRoot(MyApp),

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    InvitacionPage,
    AlertasPage,
    NotificacionesPage,
    ReservacionPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
